# groupedTest

In this simulations, we assume that a disease is present with probability p
in the popultion. We can pick an arbitrary number of samples from the popultion
and mix several of them. When a test is performed on a mix of N different
samples, this tests is positive if at least one of the original sample has the 
disease. 

Quick conclusion: when p<0.2, we should clear test multiple persons at the same
time. This is even more true when N and p are small. 
